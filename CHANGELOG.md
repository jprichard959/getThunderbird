# changelog getThunderbird

tags utilisés: added  changed  cosmetic  deprecated  fixed  new rewriting  removed  syncro  security
date au format: YYYY-MM-DD

## [ Unreleased ]


## [ 2.14.1 ] - 2019.07.08

* rewriting f__wget_test
* rewriting: récup versions on ligne: f_tb_get_versions
* syncro: fscript_install, fscript_remove, fscript_update, f__user

## [ 2.12.0 ] - 2019.05.4

* fixed: bash v5 utilisable (v4+)

## [ 2.11.0 ] - 2018.07.3

* synchro: f__requis, f__sort_uniq

## [ 2.10.1 ] - 2018.06.13

* added: f__read_file
* rewriting: suppression sed peu utiles
* rewriting: f__archive_test
* cosmetic: exit sur --help

## [ 2.10.0 ] - 2018.06.12

* syncro: composants, fscript_update avec ses parametres d'appel
* cosmetic;
* rewriting: f__sudo, plus de multiples tentatives pour su, plus simple et fix potentiel stderr non visible
* fix: f_tb_install, effacement correct archive téléchargée
* fix: droits corrects sur téléchargement si root
* fix: potentiel, droits si téléchargement et mauvais effacement précédent

## [ 2.9.2 ] - 2018.06.11

* syncro: composants, fscript_update avec ses parametres d'appel
* cosmetic:

## [ 2.9.1 ] - 2018.06.10

* added: options --dev
* added: option téléchargement seul
* added: f_tb_install, version archives utilisées, téléchargées, installées
* added: f_tb_get_versions, assigne version latest version beta
* added: f__trim
* rewriting: traitement options d'appel
* rewriting: shellcheck
* cosmetic: affiche help & affichage
* syncro: composants
* fixed: f_tb_lanceur_desktop, plusieurs versions simultanées possibles, réparer avec `getThunderbird ri`
* fixed: typo
* fixed; effacement fichier temporaire si téléchargement

## [ 2.8.0 ] - 2018.06.06

###  publication sur sdeb/getThunderbird, historique abandonné

## [ 2.7.0 ] - 2018.03.04

* syncro: f__color, f__info, f__sudo, f__user, f__wget_test
* syncro: fscript_install, fscript_remove, fscript_update
* rewriting: prg_init, f_help
* rewriting: général ubuntu 16.04, 

## [ 2.6.2 ] - 2018.02.11

* syncro: f__color

## [ 2.6.1 ] - 2018.01.29

* rewriting: +requis awk>gawk

## [ 2.6.0 ] - 2018.01.26

* rewriting: mineur, fscript_cronAnacron fscript_install fscript_remove fscript_update
* rewriting: f__requis
* fixed: f__sudo, extraction nb tentatives

## [ 2.4.0 ] - 2018.01.24

* rewriting: f_help, f_affichage
* rewriting: ffx_install
* rewriting: général wget_log: fscript_get_version, fscript_update

## [ 2.3.0 ] - 2018.01.16

* added: cumul options (opérations) possibles (sauf opés scripts)
* added: option --sauve pour conserver le téléchargement à l'installation
* rewriting; f__wget_test
* rewriting: f_sudo abandonné dans fscript_install et fscript_remove, au profit appel au traitement général des options
* rewriting: général, invocation f__sudo dans traitement options, plus confortable si su & _all_
* rewriting: f_sudo, format nombre de tentatives et options appel possibles > 1
* rewriting: $appli en global au lieu de local
* rewriting: menu réparation icône pour tous les canaux installés, menu help en conséquence
* rewriting: auto-installation, potentiel bug selon conditions appel
* fixed: options all

## [ 2.2.0 ] - 2018.01.12

* fixed: correction commentaire fscript_get_version

## [ 2.1.0 ] - 2017.12.29

* syncro: composants

## [ 2.0.0 ] - 2017.12.27

* cosmetic: remodelage complet (mise à niveau sur getFirefox)

## [ 1.10.0 ] - 2017.12.19

* rewriting: lanceur desktop

## [ 1.9.0 ] - 2017.12.18

* fixed: install manuelle, bug potentiel

## [ 1.8.1 ] - 2017.12.15

* syncro: f__architecture

## [ 1.8.0 ] - 2017.12.13

* rewriting: f__wget_test

## [ 1.7.0 ] - 2017.12.6

* rewriting: fscript_update, controle chargement début et fin
* rewriting: changement séquence start pour éviter erreur cron

## [ 1.6.1 ] - 2017.12.5

* syncro: fonctions communes
* rewriting: démarrage
* rewriting: renommage $architecture
* rewriting: f_tb_alertIcedove, f_tb_default, f_tb_install, f_tb_profil_user, fscript_cronAnacron_special, renommage user_
* rewriting: suppression avertissement paquet debian, compatibilité
* rewriting: f_tb_get_versions
* fixed: f__wget_test

## [ 1.5.0 ] - 2017.10.16

* rewriting: f__error f__info f__requis f__wget_test 

## [ 1.4.0 ] - 2017.10.11

* fixed: f__sudo : fonctionnement avec sudo

## [ 1.3.0 ] - 2017.10.08

* added: f__wget_test(): nouvelle option test, nouveau nommage fichier temp
* rewriting: f__user, premier essai root only, fonctionnement en root only en console
* changed: test bash4 au démarrage
* rewriting: f__color: utilisation terminfo pour retour au std (et non noir), donc modifs:
* syncro: f__color f__error f__info f__wget_test, fscript_get_version fscript_install fscript_remove fscript_update
* added: intégration f__sudo dans install & remove script

## [ 1.2.0 ] - 2017.09.24

* fixed: bug install

  /!\ nécessite une réinstallation manuelle

## [ 1.1.1 ] - 2017.09.23

* syncro: f__requis, f__info, f__error
* rewriting: unset/for

## [ 1.0.0 ] - 2017.09.09

* 1ère publication
